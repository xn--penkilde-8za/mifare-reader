cmake_minimum_required(VERSION 3.19)

CMAKE_POLICY(SET CMP0071 NEW)

project(mifare-reader VERSION 1.0.0 LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()

set(CMAKE_CXX_FLAGS "-Wall -Wextra -flto")
set(CMAKE_CXX_FLAGS_RELEASE "-O3")

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++20")

find_package(Threads REQUIRED)
find_package(OpenSSL REQUIRED)
find_package(Qt6 COMPONENTS Widgets REQUIRED)

message(STATUS "Using OpenSSL ${OpenSSL_VERSION}")
message(STATUS "Using Qt ${Qt6_VERSION}")

set(
  PROJECT_SOURCES
    sources/main.c++
    sources/qt/main_window.h++
    sources/qt/main_window.ui
    sources/qt/resources.qrc
    sources/qt/nfc_worker.c++
    sources/qt/nfc_worker.h++
    sources/pcsc/context.h++
    sources/pcsc/context.c++
    sources/pcsc/reader.h++
    sources/pcsc/reader.c++
    sources/pcsc/card.h++
    sources/pcsc/card.c++
    sources/pcsc/pcsc.h++
    sources/desfire/desfire_crypto.c++
    sources/desfire/desfire_crypto.h++
)

add_executable(${PROJECT_NAME} ${PROJECT_SOURCES})

# PC/SC API dependencies.
add_library(pcsc INTERFACE)
find_package(PkgConfig)
pkg_check_modules(PCSC libpcsclite)
target_include_directories(${PROJECT_NAME} PRIVATE ${PCSC_INCLUDE_DIRS})
target_link_libraries(pcsc INTERFACE ${PCSC_LIBRARIES})

target_link_libraries(
  ${PROJECT_NAME}
    PRIVATE
    Qt6::Widgets
    pcsc
    OpenSSL::SSL
)
