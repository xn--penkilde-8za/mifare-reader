# Mifare Reader in C++ and Qt

This project is a very stripped down version of a validator for a public transport company in Norway.
It served as a reference implementation for validating travellers cards.

Sadly, due to a third party, the entire code could not be open sourced, so those parts have been removed from this version.

There are however some interesting parts left: 
- The pcsc folder holds a general library for interfacing with pcsc readers.
- The desfire folder holds c++ crypto code that can read an encrypted travel card (if you have the right key)

## Notes
This code is currently a 'special-case', since:
- Only works on Linux
- It has very little error-handling.
- Only support Plug n' Play mode for nfc-readers.
- Always selects the first card reader discovered.

This code should be generalized more, but for now, this is what we need.

## Building

To build, first make sure that you have installed the necessarily libraries.

On **openSUSE**, this is done with:

    sudo zypper install cmake gcc-c++ pcsc-acsccid pcsc-lite-devel openssl-devel qt6-base-devel qt6-wayland 

Once that is done, this project should build as simple as:

    cmake .
    make
