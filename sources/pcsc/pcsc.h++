#ifndef PCSC_PCSC_H
#define PCSC_PCSC_H

#include <winscard.h>

namespace delirium::openkilde {
  namespace mifare_reader::pcsc {
    using reader_state = SCARD_READERSTATE;

    using io_request = SCARD_IO_REQUEST;
  } // namespace mifare_reader::pcsc
} // namespace delirium::openkilde

#endif // PCSC_PCSC_H
