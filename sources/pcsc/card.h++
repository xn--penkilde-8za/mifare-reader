#ifndef PCSC_CARD_H
#define PCSC_CARD_H

#include <memory>
#include <vector>

#include "pcsc.h++"

namespace delirium::openkilde {
  namespace mifare_reader::pcsc {
    class reader; // forward incomplete class

    class response; // forward incomplete class

    class card {
      public:
        card(long card_handle_, unsigned long card_protocol_);

        ~card();

        std::string id();

        void buzz();

        void buzz_off();

        std::vector<unsigned char> read_auto_pps();

        std::tuple<bool, std::vector<unsigned char>> transceive(
            std::vector<unsigned char>* frame_);

        std::tuple<bool, std::vector<unsigned char>> transceive_with_af(
            std::vector<unsigned char> data_);

      private:
        long card_handle_;

        io_request request_;

        std::vector<unsigned char> response_data_;

        unsigned long response_length_ = 258;
    };
  } // namespace mifare_reader::pcsc
} // namespace delirium::openkilde

#endif // PCSC_CARD_H
