#ifndef PCSC_CONTEXT_H
#define PCSC_CONTEXT_H

#include <memory>
#include <vector>

#include <PCSC/pcsclite.h>

namespace delirium::openkilde {
  namespace mifare_reader::pcsc {
    class reader; // forward incomplete class

    class context {
      public:
        context();

        ~context();

        long* handle();

        std::vector<reader>* readers();

      protected:
        void free_readers_memory();

      private:
        long handle_;

        std::vector<reader> readers_;

        char* identifiers_;
    };
  } // namespace mifare_reader::pcsc
} // namespace delirium::openkilde

#endif // PCSC_CONTEXT_H
