#include <winscard.h>

#include "card.h++"
#include "context.h++"
#include "reader.h++"

namespace delirium::openkilde {
  namespace mifare_reader::pcsc {
    reader::reader(context* context_, reader_state state_)
        : context_(context_),
          state_(state_) {
    }

    const char* reader::name() { return this->state_.szReader; }

    std::unique_ptr<card> reader::wait_for_card() {
      auto result_ = SCARD_S_SUCCESS;

      result_ = SCardGetStatusChange(
          *this->context_->handle(),
          INFINITE,
          &this->state_,
          1);

      if (result_ == SCARD_S_SUCCESS && this->state_.dwEventState & SCARD_STATE_CHANGED) {
        this->state_.dwCurrentState = this->state_.dwEventState;

        if (this->state_.dwEventState & SCARD_STATE_PRESENT && !(this->state_.dwEventState & SCARD_STATE_MUTE)) {
          auto card_protocol_{0ul};

          auto card_handle_{0l};

          result_ = SCardConnect(
              *this->context_->handle(),
              this->state_.szReader,
              SCARD_SHARE_SHARED,
              SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1, // Let PCSC auto-select protocol.
              &card_handle_,
              &card_protocol_);

          if (result_ == SCARD_S_SUCCESS) {
            return std::make_unique<card>(card_handle_, card_protocol_);
          }
        }
      }

      return nullptr; // SCardGetStatusChange released, but no card was detected
    }
  } // namespace mifare_reader::pcsc
} // namespace delirium::openkilde