#include <iostream>

#include <winscard.h>
#include <chrono>

#include "card.h++"

namespace delirium::openkilde {
  namespace mifare_reader::pcsc {
    card::card(long card_handle_, unsigned long card_protocol_)
        : card_handle_(card_handle_),
          request_({.dwProtocol = card_protocol_, .cbPciLength = sizeof(request_)}),
          response_data_(std::vector<unsigned char>(258)) {
      SCardBeginTransaction(this->card_handle_);
    }

    card::~card() {
      if (this->card_handle_) {
        SCardEndTransaction(this->card_handle_, SCARD_EJECT_CARD);

        SCardDisconnect(this->card_handle_, SCARD_EJECT_CARD);

        this->card_handle_ = 0;
      }
    }

    std::tuple<bool, std::vector<unsigned char>> card::transceive(
        std::vector<unsigned char>* frame_) {
      this->response_length_ = 258;

      auto result = SCardTransmit(
          this->card_handle_,
          &this->request_,
          frame_->data(),
          frame_->size(),
          nullptr,
          response_data_.data(),
          &this->response_length_);

      return std::make_tuple(
          result == SCARD_S_SUCCESS,
          std::vector(
              std::begin(this->response_data_),
              std::begin(this->response_data_) + this->response_length_));
    }

    std::tuple<bool, std::vector<unsigned char>> card::transceive_with_af(std::vector<unsigned char> frame_) {
      auto exit_ = false;

      auto super_result_ = std::vector<unsigned char>{0}; // make room for status

      do {
        auto [success_, result_] = this->transceive(&frame_);

        if (success_) {
          super_result_.insert(super_result_.end(), result_.begin() + 1, result_.end());

          auto status_ = (unsigned char) result_[0];

          if (status_ != (unsigned char) 0xAF) {
            super_result_[0] = status_;

            return std::make_tuple(success_, super_result_);
          }

          frame_ = std::vector<unsigned char>{0xAF};
        } else {
          exit_ = true;
        }
      } while (!exit_);

      return std::make_tuple(false, super_result_);
    }

    std::string card::id() {
      auto bytes_ = std::vector<unsigned char>{
          0xFF, 0xCA, 0x00, 0x00, 0x00
      };

      auto uid_ = uint_fast64_t{0};

      auto [success_, result_] = this->transceive(&bytes_);

      if (success_) {
        for (auto i_{0ul}; i_ != result_.size() - 2; i_++) {
          uid_ <<= 8;
          uid_ |= static_cast<uint64_t>(result_[i_]);
        }
      }

      return std::to_string(uid_);
    }

    void card::buzz() {
      auto bytes_ = std::vector<unsigned char>{
          0xFF, 0x00, 0x40, 0x00, 0x4C, 0x01, 0x00, 0x01, 0x01
      };

      this->transceive(&bytes_);
    }

    std::vector<unsigned char> card::read_auto_pps() {
      auto bytes_ = std::vector<unsigned char>{
          0xE0, 0x00, 0x00, 0x24, 0x00
      };

      auto [success_, result_] = this->transceive(&bytes_);

      if (success_) {
        return result_;
      }

      return {};
    }

    void card::buzz_off() {
      auto bytes_ = std::vector<unsigned char>{
          0xFF, 0x00, 0x52, 0x00, 0x00
      };

      this->transceive(&bytes_);
    }
  } // namespace mifare_reader::pcsc
} // namespace delirium::openkilde

