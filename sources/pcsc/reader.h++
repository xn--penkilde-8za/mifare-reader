#ifndef PCSC_READER_H
#define PCSC_READER_H

#include <cstring>
#include <memory>

#include <pcsclite.h>

#include "pcsc.h++"

namespace delirium::openkilde {
  namespace mifare_reader::pcsc {
    class context; // forward incomplete class

    class card; // forward incomplete class

    class reader {
      public:
        reader(context* context_, reader_state state_);

        const char* name();

        std::unique_ptr<card> wait_for_card();

      private:
        context* context_;

        reader_state state_;
    };
  } // namespace mifare_reader::pcsc
} // namespace delirium::openkilde

#endif // PCSC_READER_H
