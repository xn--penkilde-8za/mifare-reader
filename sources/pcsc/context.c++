#include <cstring>
#include <iostream>

#include <winscard.h>

#include "context.h++"
#include "reader.h++"

namespace delirium::openkilde {
  namespace mifare_reader::pcsc {
    context::context()
        : handle_(0ul),
          identifiers_(nullptr) {
      auto result_ = SCardEstablishContext(
          SCARD_SCOPE_SYSTEM,
          nullptr,
          nullptr,
          &this->handle_);

      if (result_ != SCARD_S_SUCCESS) {
        SCardReleaseContext(this->handle_);

        throw std::runtime_error("SCardEstablishContext failed");
      }
    }

    context::~context() {
      if (this->identifiers_) {
        this->free_readers_memory();
      }

      SCardReleaseContext(this->handle_);
    }

    long* context::handle() { return &this->handle_; }

    std::vector<reader>* context::readers() {
      this->free_readers_memory();

      auto identifiers_length_ = SCARD_AUTOALLOCATE;

      SCardListReaders(this->handle_, nullptr, (LPTSTR) &this->identifiers_, &identifiers_length_);

      if (this->identifiers_ == nullptr) {
        throw std::runtime_error("could not find any readers");
      }

      auto states_ = std::make_unique<std::vector<reader_state>>();

      for (auto* identifier_ = this->identifiers_; *identifier_; identifier_ += strlen(identifier_) + 1) {
        states_->push_back(
            {
                identifier_,
                nullptr,
                SCARD_STATE_UNAWARE,
                SCARD_STATE_UNAWARE,
                0,
                {
                    0,
                }
            }
        );
      }

      SCardGetStatusChange(
          this->handle_,
          0,
          states_->data(),
          states_->size());

      for (auto &state_: *states_) {
        this->readers_.emplace_back(this, state_);
      }

      return &this->readers_;
    }

    void context::free_readers_memory() {
      if (this->handle_ && this->identifiers_) {
        SCardFreeMemory(
            this->handle_,
            this->identifiers_);

        this->identifiers_ = nullptr;

        this->readers_.clear();
      }
    }
  } // namespace mifare_reader::pcsc
} // namespace delirium::openkilde