#include <iostream>
#include <cstring>

#include "desfire_crypto.h++"

namespace delirium::openkilde {
  namespace mifare_reader::desfire {
    desfire_crypto::desfire_crypto(std::vector<unsigned char> key_)
        : ks1_(DES_key_schedule{}),
          ks2_(DES_key_schedule{}),
          response_data_(std::vector<unsigned char>(16)),
          pcd_rnd_a_(std::vector<unsigned char>(16)),
          picc_e_rnd_b_(std::vector<unsigned char>(8)),
          pcd_r_rnd_b_(std::vector<unsigned char>(8)),
          token_(std::vector<unsigned char>(32)),
          buffer_(33, 0xAF) {
      DES_set_key((DES_cblock*) key_.data(), &this->ks1_);

      DES_set_key((DES_cblock*) (key_.data() + 8), &this->ks2_);
    }

    std::vector<unsigned char> desfire_crypto::authenticate(std::vector<unsigned char> data_) {
      std::fill(this->response_data_.begin(), this->response_data_.end(), 0);

      this->picc_e_rnd_b_.clear();

      this->pcd_r_rnd_b_.clear();

      RAND_bytes(this->pcd_rnd_a_.data(), 16);

      auto key_length_ = data_.size() - 1;

      auto data_first = data_.begin() + 1;

      auto data_last = data_first + key_length_;

      std::copy(data_first, data_last, std::back_inserter(this->picc_e_rnd_b_));

      mifare_cypher_blocks_chained(picc_e_rnd_b_.data(), key_length_, MCD_RECEIVE);

      std::copy(picc_e_rnd_b_.begin(), picc_e_rnd_b_.begin() + key_length_, std::back_inserter(this->pcd_r_rnd_b_));

      rol_block(this->pcd_r_rnd_b_.data(), key_length_);

      for (unsigned i = 0; i < key_length_; i++) {
        token_.at(i) = this->pcd_rnd_a_[i];
      }

      for (unsigned i = 8; i < 2 * key_length_; i++) {
        token_.at(i) = this->pcd_r_rnd_b_[i - key_length_];
      }

      mifare_cypher_blocks_chained(token_.data(), 2 * key_length_, MCD_SEND);

      for (unsigned i = 0; i < token_.size(); i++) {
        buffer_.at(i + 1) = this->token_[i];
      }

      return {buffer_.begin(), buffer_.begin() + 17};
    }

    void desfire_crypto::mifare_cypher_blocks_chained(uint8_t* data_, size_t data_size_, mifare_crypto_direction direction_) {
      size_t offset_ = 0;

      while (offset_ < data_size_) {
        mifare_cypher_single_block(data_ + offset_, direction_);

        offset_ += this->block_size_;
      }
    }

    void desfire_crypto::mifare_cypher_single_block(uint8_t* data_, mifare_crypto_direction direction_) {
      uint8_t ovect_[16];

      if (direction_ == MCD_SEND) {
        xor_block(data_);
      } else {
        memcpy(ovect_, data_, this->block_size_);
      }

      uint8_t edata_[16];

      DES_ecb_encrypt((DES_cblock*) data_, (DES_cblock*) edata_, &this->ks1_, DES_DECRYPT);

      DES_ecb_encrypt((DES_cblock*) edata_, (DES_cblock*) data_, &this->ks2_, DES_ENCRYPT);

      DES_ecb_encrypt((DES_cblock*) data_, (DES_cblock*) edata_, &this->ks1_, DES_DECRYPT);

      memcpy(data_, edata_, this->block_size_);

      if (direction_ == MCD_SEND) {
        memcpy(this->response_data_.data(), data_, this->block_size_);
      } else {
        xor_block(data_);

        memcpy(this->response_data_.data(), ovect_, this->block_size_);
      }
    }

    void desfire_crypto::xor_block(uint8_t* data) {
      for (size_t i = 0; i < this->block_size_; i++) {
        data[i] ^= this->response_data_[i];
      }
    };

    void desfire_crypto::rol_block(uint8_t* data_, size_t length_) {
      uint8_t first = data_[0];

      for (size_t i = 0; i < length_ - 1; i++) {
        data_[i] = data_[i + 1];
      }

      data_[length_ - 1] = first;
    }
  } // namespace mifare_reader::desfire
} // namespace delirium::openkilde