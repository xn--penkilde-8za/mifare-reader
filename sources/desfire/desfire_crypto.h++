#ifndef DESFIRE_CRYPTO_H
#define DESFIRE_CRYPTO_H

#include <vector>

#include <openssl/des.h>
#include <openssl/rand.h>

namespace delirium::openkilde {
  namespace mifare_reader::desfire {
    typedef enum {
      MCD_SEND, MCD_RECEIVE
    } mifare_crypto_direction;

    class desfire_crypto {
      public:
        desfire_crypto(std::vector<unsigned char> key_);

        std::vector<unsigned char> authenticate(
            std::vector<unsigned char> data_);

      protected:
        void mifare_cypher_blocks_chained(
            uint8_t* data_,
            size_t data_size_,
            mifare_crypto_direction direction_);

        void mifare_cypher_single_block(
            uint8_t* data_,
            mifare_crypto_direction direction_);

        void xor_block(
            uint8_t* data_);

        static void rol_block(
            uint8_t* data_,
            size_t length_);

      private:
        DES_key_schedule ks1_;

        DES_key_schedule ks2_;

        std::vector<unsigned char> response_data_;

        std::vector<unsigned char> pcd_rnd_a_;

        std::vector<unsigned char> picc_e_rnd_b_;

        std::vector<unsigned char> pcd_r_rnd_b_;

        std::vector<unsigned char> token_;

        std::vector<unsigned char> buffer_;

        size_t block_size_ = 8;
    };
  } // namespace mifare_reader::desfire
} // namespace delirium::openkilde

#endif // DESFIRE_CRYPTO_H
