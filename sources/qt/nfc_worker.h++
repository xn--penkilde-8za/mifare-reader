#ifndef QT_NFC_WORKER_H
#define QT_NFC_WORKER_H

#include <memory>
#include <regex>
#include <thread>

#include "qcolor.h"
#include <QObject>

#include "sources/pcsc/card.h++"

namespace delirium::openkilde {
  namespace mifare_reader {
    namespace desfire {
      class desfire_crypto; // forward incomplete class
    }

    namespace pcsc {
      class context; // forward incomplete class

      class reader; // forward incomplete class
    }

    namespace qt {
      class nfc_worker : public QObject {
        Q_OBJECT

        public:
          nfc_worker();

        public slots:

          void process();

        signals:

          void finished();

          void error(QString error_);

          void reprocess();

          void send_notification(QString id_);

          void reset();

          void progress(int progress_);

          void status(QColor color_);

        protected:
          std::tuple<bool, std::string> token_id();

          std::string travel_card_id();

          static std::string to_hex_string(
              std::vector<unsigned char> &bytes);

        private:
          bool buzzer_off_ = false;

          std::shared_ptr<pcsc::context> context_;

          pcsc::reader* reader_;

          desfire::desfire_crypto* desfire_crypto_;

          std::unique_ptr<pcsc::card> card_;

          std::regex* uuid_regex_;

          std::string correlation_id_;
      };
    } // namespace qt
  } // namespace mifare_reader
} // namespace delirium::openkilde
#endif // QT_NFC_WORKER_H
