#include <iomanip>
#include <future>
#include <regex>

#include "sources/desfire/desfire_crypto.h++"
#include "sources/pcsc/context.h++"
#include "sources/pcsc/reader.h++"

#include "nfc_worker.h++"

namespace delirium::openkilde {
  namespace mifare_reader::qt {
    nfc_worker::nfc_worker() {
      this->context_ = std::make_shared<pcsc::context>();

      this->reader_ = this->context_->readers()->data();
                                                                          /*insert correct crypto key here*/
      auto key_data_ = std::vector<unsigned char>{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

      this->uuid_regex_ = new std::regex("([0-9a-fA-F]{8})([0-9a-fA-F]{4})([0-9a-fA-F]{4})([0-9a-fA-F]{4})([0-9a-fA-F]+)");

      this->desfire_crypto_ = new desfire::desfire_crypto(key_data_);

      nfc_worker::connect(this, SIGNAL(reprocess()), this, SLOT(process()));
    }

    void nfc_worker::process() {
      this->card_ = this->reader_->wait_for_card(); // blocks execution

      if (this->card_) {
        // if you have the correct key, uncomment the lines below
        /*auto [token_success_, token_id_] = this->token_id();

        if (token_success_) {
          emit this->send_notification(QString::fromStdString("Token: " + token_id_));
        } else {
          auto card_id_ = this->travel_card_id();

          emit this->send_notification(QString::fromStdString("Kortnummer: " + card_id_));
        }*/

        auto card_id_ = this->travel_card_id();

        emit this->send_notification(QString::fromStdString("Kortnummer: " + card_id_));

        if (!this->buzzer_off_) {
          this->card_->buzz_off();

          this->buzzer_off_ = true;
        }
      } else {
        emit this->reset();

        emit this->progress(0);
      }

      emit reprocess();
    }

    std::tuple<bool, std::string> nfc_worker::token_id() {
      auto select_application_command_ = std::vector<unsigned char>{
          0x5A, 0x01, 0x80, 0x57 // select application 578001
      };

      auto [application_success_, result_] = this->card_->transceive(&select_application_command_);

      if (application_success_) {
        auto select_key_command_ = std::vector<unsigned char>{
            0x0A, 0x07 // select key 7
        };

        auto [key_success_, challenge_] = this->card_->transceive(&select_key_command_);

        if (key_success_) {
          auto unlocked_key_ = this->desfire_crypto_->authenticate(challenge_);

          auto [unluck_success_, _] = this->card_->transceive(&unlocked_key_);

          auto read_file_command_ = std::vector<unsigned char>{
              0xBD, 0x01, 0x00, 0x00, 0x00, 0x80, 0x01, 0x00 // read file 1
          };

          auto [read_file_success_, file_] = this->card_->transceive_with_af(read_file_command_);

          if (read_file_success_) {
            /* This is a shortcut to get the token.
             * It only works for travel-cards that has one fixed application.
             * The correct approach is to travel through the pointers and look up the
             * real address of cardContract.
             */
            std::string token_ = to_hex_string(file_).substr(31, 32);

            return std::make_tuple(true, std::regex_replace(token_, *this->uuid_regex_, "$1-$2-$3-$4-$5"));
          }
        }
      }

      return {};
    }

    std::string nfc_worker::travel_card_id() {
      auto bytes_ = std::vector<unsigned char>{
          0x5A, 0x00, 0x80, 0x57 // select application 578000
      };

      auto [success_, result_] = this->card_->transceive(&bytes_);

      if (success_) {
        bytes_ = std::vector<unsigned char>{
            0xBD, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 // read file C
        };

        auto [success_, result_] = this->card_->transceive(&bytes_);

        if (success_) {
          auto travel_card_id_{0l};
          // CarIdNumber32bits field offset 32 bits in CI_HEADER file
          travel_card_id_ |= (result_[5]) & 0xFF;
          travel_card_id_ <<= 8;
          travel_card_id_ |= (result_[6]) & 0xFF;
          travel_card_id_ <<= 8;
          travel_card_id_ |= (result_[7]) & 0xFF;
          travel_card_id_ <<= 8;
          travel_card_id_ |= (result_[8]) & 0xFF;

          return std::to_string(travel_card_id_);
        }
      }

      return {};
    }

    std::string nfc_worker::to_hex_string(std::vector<unsigned char> &bytes_) {
      std::ostringstream hex_string_builder_;

      hex_string_builder_ << std::setfill('0') << std::hex;

      for (auto byte_: bytes_) {
        hex_string_builder_ << std::setw(2) << short(byte_);
      }

      return hex_string_builder_.str();
    }
  } // namespace mifare_reader::qt
} // namespace delirium::openkilde
