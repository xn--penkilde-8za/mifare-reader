#ifndef QT_MAINWINDOW_H
#define QT_MAINWINDOW_H

#include <iostream>

#include <QMainWindow>
#include <QMovie>
#include <QPropertyAnimation>
#include <QThread>

#include "./ui_main_window.h"
#include "nfc_worker.h++"

QT_BEGIN_NAMESPACE
namespace Ui {
  class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
  Q_OBJECT

  private slots:
    void show_notification(QString text_) {
      this->ui_->textEdit->append(text_);

      this->ui_->textEdit->append("");
    }

    void clear() {
      this->ui_->textEdit->clear();

      this->ui_->progressBar->setValue(progress_);

      this->progress_ = 0;

      this->set_status_color(QColor::fromRgb(253, 209, 4)); // Yellow
    }

    void set_status_color(QColor color_) {
      auto palette_ = this->ui_->progressBar->palette();

      palette_.setColor(QPalette::Highlight, color_);

      this->ui_->progressBar->setPalette(palette_);
    }

    void set_progressbar(int progress_) {
      if (progress_ == 0) {
        this->ui_->progressBar->setValue(progress_);

        this->progress_ = 0;
      } else {
        auto* animation_ = new QPropertyAnimation(this->ui_->progressBar, "value");
        animation_->setDuration(250);
        animation_->setStartValue(this->progress_);
        animation_->setEndValue(progress_);
        animation_->start();

        this->progress_ = progress_;
      }
    }

  public:
    MainWindow(QWidget* parent = nullptr) : QMainWindow(parent), ui_(std::make_unique<Ui::MainWindow>()) {
      this->ui_->setupUi(this);

      auto nfc_thread_ = new QThread{};

      this->nfc_worker_ = new delirium::openkilde::mifare_reader::qt::nfc_worker{};

      this->nfc_worker_->moveToThread(nfc_thread_);

      MainWindow::connect(nfc_thread_, SIGNAL(started()), nfc_worker_, SLOT(process()));
      MainWindow::connect(nfc_worker_, SIGNAL(finished()), nfc_thread_, SLOT(quit()));
      MainWindow::connect(nfc_worker_, SIGNAL(finished()), nfc_worker_, SLOT(deleteLater()));
      MainWindow::connect(nfc_thread_, SIGNAL(finished()), nfc_thread_, SLOT(deleteLater()));
      MainWindow::connect(nfc_worker_, SIGNAL(send_notification(QString)), this, SLOT(show_notification(QString)));
      MainWindow::connect(nfc_worker_, SIGNAL(reset()), this, SLOT(clear()));
      MainWindow::connect(nfc_worker_, SIGNAL(progress(int)), this, SLOT(set_progressbar(int)));
      MainWindow::connect(nfc_worker_, SIGNAL(status(QColor)), this, SLOT(set_status_color(QColor)));

      nfc_thread_->start();
    }

  private:
    int progress_ = 0;

    std::unique_ptr<Ui::MainWindow> ui_;

    delirium::openkilde::mifare_reader::qt::nfc_worker* nfc_worker_;
};

#endif // QT_MAINWINDOW_H
