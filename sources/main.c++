#include <QApplication>

#include "qt/main_window.h++"

int main(int argc_, char* argv_[]) {
  auto application_ = QApplication{argc_, argv_};

  auto window_ = MainWindow{};

  window_.show();

  return QApplication::exec();
}